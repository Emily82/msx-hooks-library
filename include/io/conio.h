#ifndef  CONIO_H
#define  CONIO_H

#include "types.h"
#include "bios.h"

void puts(char *s);
void puthex8(uint8_t v);
void puthex16(uint16_t v);
void putdec8(uint8_t v);
void putdec16(uint16_t v);


#endif  // CONIO_H
