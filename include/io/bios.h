#ifndef BIOS_H
#define BIOS_H
	void cls(void);
	void chkram(void);
	void putchar(char c);
	char getchar(void);
	void beep(void);
#endif
