#ifndef HELPER_H
#define HELPER_H

#include <types.h>

//This is a macro to use when we need to skip hooked routine execution
#define	SKIP_ROUTINE	__asm\
			pop hl\
			pop af\
			pop bc\
			pop de\
			pop hl\
			__endasm


typedef void (*fnPtr)(void);


#endif
