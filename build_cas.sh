#!/bin/sh
INCLUDE="-I include/
		 -I include/io"
SRC="src"
OUTPUT="build"
STD="--std-c99"

rm -rf $OUTPUT
mkdir -p $OUTPUT
mkdir -p $OUTPUT/cas

sdasz80 -o $OUTPUT/crt0cas.rel $SRC/crt0cas.s
sdasz80 -o $OUTPUT/bios.rel $SRC/bios.s
sdcc $INCLUDE -mz80 -c -o $OUTPUT/helper.rel $SRC/helper.c $STD
sdcc $INCLUDE -mz80 -c -o $OUTPUT/hooks.rel $SRC/hooks.c $STD
sdcc $INCLUDE -mz80 -c -o $OUTPUT/conio.rel $SRC/conio.c $STD
sdcc $INCLUDE -mz80 -c -o $OUTPUT/main.rel $SRC/main.c $STD

sdldz80 -nf linker.lk

objcopy -Iihex -Obinary $OUTPUT/main.ihx $OUTPUT/cas/hook.cas

