# Msx Hooks C library

This is my attempt to create a library to program an msx hook from C language, using sdcc cross compiler.
There is already a test example that alters "print <var>" basic instruction.
There should be a beep for every print <var> you do :)

### Building
- To build the test library as cassette for msx emulator just run the script file

        ./build_cas.sh
NOTE: this will make a .cas file in build/cas folder, to be used for any emulator

- If you need .bin file to be used inside a floppy image

        ./build_bin.sh
NOTE: if you are using OpenMSX you can quickly use the feature "Browse dir as disk" to
load the binary file.

Inside build folder there will be "cas" folder or "dsk" folder depending on what kind of build you have choosen.

IMPORTANT: If you need to build binary file for floppy disk, remember to edit "linker.lk"
replacing the line containing "build/crt0cas.rel" with "build/crt0bin.rel" otherwise the build will fail.

If you want you can also decrease the size of the executable header in "-b _CODE = ..."
I used a big number because cassette needs big header. This is different for simple binaries for floppy disks.

### Loading

It's important to remember to allocate space for test routine, avoiding to overwrite Basic user space.
So before doing any "bload" you have to type this from basic
		CLEAR 200, &h9FFF


Have Fun! I hope this being usefull tool for MSX Resource Center ;)
