#include <conio.h>
#include <hooks.h>
#include <types.h>
#include <helper.h>
#include <bios.h>
// #pragma	preproc_asm +

void onCursorDelete(void);

//Startup code
void main() {
	programHook((uint8_t *)H_ERAC, onCursorDelete);
}

void onCursorDelete(void) {
	//When we need to skip hooked routine execution, just call this macro
	SKIP_ROUTINE;
 	beep();
}
