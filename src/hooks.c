#include <hooks.h>
#include <conio.h>

void programHook(uint8_t *hook, fnPtr hookIntercept)
{
	uint16_t address = (uint16_t)hookIntercept;

	//We need to write an unconditioned jump to hookIntercept address
 	hook[0] = 0xC3;	//JP
 	hook[1] = (uint8_t)(address & 0x00ff); //First LSB
 	hook[2] = (uint8_t)((address & 0xff00) >> 8); //Last MSB (Z80 is LittleEndian)

}
