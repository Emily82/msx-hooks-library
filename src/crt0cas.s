	;--- crt0.asm for MSX-BASIC - by Konamiman, 1/2018
	;
	;    Change the .org directive as appropriate for your program
	;
	;    To assemble it:
	;    sdasz80 -o crt0_msxbasic.rel crt0_msxbasic.s
	;
	;    Compile programs with --code-loc <org address + 32> --data-loc X
	;    X=0  -> global vars will be placed immediately after code
	;    X!=0 -> global vars will be placed at address X

	;	 This header has been customized to generate .cas file with binary inside :D - by Emily82 -
	;	 For cas format only the header needs 32 bytes of space, so compile with
	;	 --code-loc <org address + 84> to be sure

	.module crt0
	.globl	_main

    .globl  l__INITIALIZER
    .globl  s__INITIALIZED
    .globl  s__INITIALIZER

	.area _HEADER (ABS)

	.org    0xA000
	.db 	0x1F, 0xA6, 0xDE, 0xBA, 0xCC, 0x13, 0x7D, 0x74
    .db 	0xD0, 0xD0, 0xD0, 0xD0, 0xD0, 0xD0, 0xD0, 0xD0, 0xD0, 0xD0
    ;	fixed 6 bytes ascii
    .ascii	"hook  "
    .db		0x1F, 0xA6, 0xDE, 0xBA, 0xCC, 0x13, 0x7D, 0x74
    .dw 	init
    .dw		end
    .dw 	init

	;--- Initialize globals and jump to "main"

init:
    ld	bc, #l__INITIALIZER
	ld	a, b
	or	a, c
	jp	z, _main
	ld	de, #s__INITIALIZED
	ld	hl, #s__INITIALIZER
	ldir

	jp    _main


	;; Ordering of segments for the linker.
	.area	_HOME
	.area	_CODE
	.area	_INITIALIZER
	.area   _GSINIT
	.area   _GSFINAL
	.area	_DATA
	.area	_INITIALIZED
	.area	_BSEG
	.area   _BSS
	.area   _HEAP

end:
	; Zero terminating byte is needed for cas
	.db		0x00
