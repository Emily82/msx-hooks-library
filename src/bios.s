	.area	_CODE
; symbols
CHGET = 0x009F
CHPUT = 0x00A2
RESET = 0x0000
BEEP  = 0x00C0


_getchar::
	push    af ; backup af register
;	Address... 10CBH
;	Name...... CHGET
;	Entry..... None
;	Exit...... A=Character from keyboard
;	Modifies.. AF, EI
;	Standard routine to fetch a character from the keyboard
;		buffer.
	call    CHGET ; call bios CHGET ( 009FH )
	ld      l,a ; write CHGET return value (i.e. a register) to _getchar return value (i.e. l register)
	ld      h,#0 ; clean h register
	pop     af ; restore af register
	ret

_putchar::
	ld      hl, #2
	add     hl, sp ;sp + 2
	ld		a, (hl)
	call    CHPUT ; call bios CHPUT ( 00A2H )
	ret

_cls::
	ld a, #0x1b
	call 	CHPUT
	ld a, #0x45
	call 	CHPUT
	ret

_chkram::
	call	RESET
	ret

_beep::
	call	BEEP
	ret

