;
;	CARTRIDGE HEADER (* 16/32KB ROM, PAGEADDR 1-2, 0x4000-0xC000)
;
;	0,	0x0-0x3FFF,	MAINROM
;	1,	0x4000-0x7FFF,	EXT ROM *
;	2,	0x8000-0xBFFF,	EXT ROM *
;	3,	0xC000-0xFFFF,	RAM(WORK AREA)
;
	.module	start
	.globl	_main
;
	.area	_ROM_HDR (ABS)
	.org	0x4000
	
	.db	'A
	.db	'B
	.dw _main
	.dw	0
	.dw	0
	.dw	0
	.dw	0
	.dw	0
	.dw	0
;
	.ascii	"END ROMHEADER"
;

