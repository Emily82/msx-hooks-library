#!/bin/sh
rm -f myhello.rom
rm -f *.bin
rm -f *.rel
rm -f *.sym
rm -f *.mem
rm -f 16kmem.*
rm -f *.ihx
rm -f *.lst
rm -f *.map
rm -f *.noi
rm -f *.asm

sdasz80 -o io.s
sdasz80 -o crt0.s
sdcc -I include/ -mz80 -c -o conio.rel conio.c
sdcc -I include/ --out-fmt-ihx --data-loc 0xc000 --code-loc 0x4020 -mz80 --no-std-crt0 crt0.rel io.rel conio.rel main.c
# sdcc --out-fmt-ihx --data-loc 0xd000 --code-loc 0xc020 -mz80 --no-std-crt0 crt0.rel io.rel main.c
makebin -s 65536 main.ihx 64k.mem
split -b 16384 64k.mem 16kmem.
mv 16kmem.ab myhello.rom

# Come binario semplice
# objcopy -Iihex -Obinary main.ihx myhello.bin
